#!/bin/python3
#
# Copyright (c) 2023 Tobias Frisch, aka TheJackiMonster
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# 

import argparse
import re
import sys

word_pattern = re.compile(r"\S+")

def main(args):
    file = args.PATH
    partCount = args.parts
    tolerance = args.tolerance
    debug = args.debug

    # read all lines from file
    lines = file.readlines()
    
    # join all lines to one string
    text = ' '.join(lines)

    # replace all manual line breaks or tabs with spaces
    text = re.sub('[\r\n\t]', ' ', text)

    # merge multiple spaces to a single one in each place
    text = re.sub(' +', ' ', text)

    # count all words in the text
    wordCount = len(word_pattern.findall(text))
    wordsPerPart = wordCount / partCount

    # split text in sentences (keep in mind the dot is missing)
    sentences = text.split('. ')

    parts = []
    for i in range(partCount):
        parts.append({ "wordCount": 0, "text": "" })

    currentPartIndex = 0

    for sentence in sentences:
        wordsInSentence = len(word_pattern.findall(sentence))

        # skip empty sentences
        if wordsInSentence <= 0:
            continue

        if currentPartIndex >= len(parts):
            currentPart = parts[-1]
        else:
            currentPart = parts[currentPartIndex]

        # sum up words for statistics
        currentPart["wordCount"] += wordsInSentence

        # combine sentences with spaces and dots to one text again
        if len(currentPart["text"]) > 0:
            currentPart["text"] += " "
        
        currentPart["text"] += sentence + "."

        # give a little wiggle room for adjustments (so that the last part isn't always the longest)
        if currentPart["wordCount"] + tolerance >= wordsPerPart:
            currentPartIndex += 1

    if debug:
        print("actual amount of words: {}".format(wordCount))
        print("ideal amount per part: {}".format(wordsPerPart))
        print()
        
        for i in range(len(parts)):
            print("part[{}] = {}".format(i, parts[i]["wordCount"]))
    else:
        pieces = map(lambda part: part["text"], parts)
        
        # join everything to one again
        text = "\n\n".join(pieces)

        print(text)

parser = argparse.ArgumentParser()
parser.add_argument('-p', '--parts', type=int, default=18, dest="parts")
parser.add_argument('-t', '--tolerance', type=int, default=3, dest="tolerance")
parser.add_argument('-d', '--debug', type=bool, default=False, dest="debug")
parser.add_argument('PATH', type=open)

if __name__ == "__main__":
    main(parser.parse_args(sys.argv[1:]))

